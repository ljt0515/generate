package ${package};

import ${beanPackage}.${beanName};
import ${servicePackage}.${serviceName};
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 *
 *${fileName}Controller
 *
 */
@Controller
public class ${fileName}{

	@Autowired
	private ${serviceName} ${serviceNames};

	/**
	 * 
	 * 查询（根据主键ID查询）
	 * 
	 */
	@ResponseBody
	@RequestMapping(value="findById")
	public ${beanName} findById (Integer id){
		return ${serviceNames}.selectByPrimaryKey(id);
	}

	/**
	 * 
	 * 删除（根据主键ID删除）
	 * 
	 */
	@ResponseBody
	@RequestMapping(value="deleteById")
	public int deleteById (Integer id){
		return ${serviceNames}.deleteByPrimaryKey(id);
	}

	/**
	 * 
	 * 添加 （匹配有值的字段）
	 * 
	 */
	@ResponseBody
	@RequestMapping(value="add")
	public int add(${beanName} record){
		return ${serviceNames}.insertSelective(record);
	}
	/**
	 * 
	 * 修改 （匹配有值的字段）
	 * 
	 */
	@ResponseBody
	@RequestMapping(value="update")
	public int update(${beanName} record){
		return ${serviceNames}.updateByPrimaryKeySelective(record);
	}
	/**
	 * 
	 * 查询（匹配有值的字段）
	 * 
	 */
	@ResponseBody
	@RequestMapping(value="search")
	public List<${beanName}> search (${beanName} record){
		return ${serviceNames}.selectByObject(record);
	}
}